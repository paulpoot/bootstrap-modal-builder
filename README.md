# Bootstrap Modal Builder (v1.0)
---
### By [Paul Poot](www.paulpoot.eu)
Last update: April 5th 2015

Bootstrap Modal Builder (from this point on referred to as BMB) is a small Javascript plugin built for Bootstrap v3.3.4 and Font-Awesome. BMB makes it significantly easier to create new modals: Include the minified script on your page, add the empty modal to your page by calling the function modalInitializer() and then build it with modalBuilder()!

### Features
  - Add basic modals to your webpages without repeating yourself.
  - Create many modals without using loads of space; you can add a new modal with just one line.
  - Font-Awesome support: just pass the name of an icon (for example: fa-github) as string "micon" and you've added an icon to the header of your modal.

### Get started

##### INITIALIZE THE MODAL AND INCLUDE THE SCRIPT
Add this to your <body> tag. Make sure it's under the actual HTML, so the structure of your page gets loaded first.

```
<!-- Bootstrap Modal Builder -->
<script src="js/plugins/bootstrap-modalbuilder/modalbuilder.min.js" type="text/javascript"></script>
<script type="text/javascript"> modalInitializer(); </script>
``` 

##### CALL THE FUNCTION
Call the function whenever you need to show a modal. 
```
modalBuilder(micon, mname, mbody, mbutton);
```
###### THE VARIABLES
Don't forget the quotation marks, every variable that gets passed to modalBuilder() is a string.

- Replace **micon** with the name of a Font-Awesome icon, for example **"fa-bitbucket"**. 
- Replace **mname** with the name of your modal.
- Replace **mbody** with the body of your modal. You can use HTML tags here, because the element with the mbody ID is a div. 
- Replace **mbutton** with the text that should appear on the button that will close the modal. I could've just let the button say "Close", but this is nice if your webpage is not in English.