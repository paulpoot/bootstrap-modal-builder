//////////////////////////////////////////////////////////
// Bootstrap Modal Builder v0.1                         //
// Built by Paul Poot (www.paulpoot.eu)                 //
//////////////////////////////////////////////////////////

function modalInitializer() {
    if (!modalInitialized) {    
        document.body.innerHTML += '<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modallabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h3 class="modal-title" id="modal-label"></h3> </div> <div id="modal-body" class="modal-body"></div> <div class="modal-footer"> <button id="modal-button" type="button" class="btn btn-block" data-dismiss="modal"></button> </div> </div> </div> </div>';
        var modalInitialized = true;
    }
}

function modalBuilder(micon, mname, mbody, mbutton) {
    document.getElementById("modal-label").innerHTML = '<i class="fa ' + micon + '"></i>' + " " + mname;
    document.getElementById("modal-body").innerHTML = mbody;
    document.getElementById("modal-button").innerHTML = mbutton;
    
    $('#modal').modal('show');
}

